package com.empresa.digital.app.java.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "usuarios")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "USERNAME", nullable = false, length = 100)
	private String username;
	
	@Column(name = "PASSWORD", nullable = false, length = 100)
	private String password;
	
	@Column(name = "EDAD", nullable = false, length = 6)
	private String edad;
	
	@Column(name = "SEXO", nullable = false, length = 20)
	private String sexo;
	
	@Column(name = "FECHA_NACIMIENTO")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@Column(name = "USER_CREATE", length = 100)
	private String userCreate;
	
	@Column(name = "DATE_CREATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreate;
	
	@Column(name = "USER_MODIFIED", length = 100)
	private String userModified;
	
	@Column(name = "DATE_MODIFIED")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateModified;

}
