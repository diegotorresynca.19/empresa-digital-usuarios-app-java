package com.empresa.digital.app.java.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empresa.digital.app.java.dto.UsuarioDTO;
import com.empresa.digital.app.java.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public UsuarioDTO save(UsuarioDTO usuario) {
		
		return usuarioService.save(usuario);
	}

	@Override
	public UsuarioDTO update(UsuarioDTO usuario) {
		
		return usuarioService.save(usuario);
	}

	@Override
	public void delete(Long id) {
		
		usuarioService.delete(id);
	}

	@Override
	public List<UsuarioDTO> findAll() {
		
		return usuarioService.findAll();
	}

	@Override
	public UsuarioDTO findById(Long id) {
		
		return usuarioService.findById(id);
	}

}
