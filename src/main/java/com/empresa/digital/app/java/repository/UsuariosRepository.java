package com.empresa.digital.app.java.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empresa.digital.app.java.models.entity.Usuario;

public interface UsuariosRepository extends JpaRepository<Usuario, Long> {

}
