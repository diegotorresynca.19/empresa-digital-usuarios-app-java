package com.empresa.digital.app.java.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class UsuarioDTO implements Serializable{

	private Long id;
	private String username;
	private String password;
	private String edad;
	private String sexo;
	private Date fechaNacimiento;
	private String userCreate;
	private Date dateCreate;
	private String userModified;
	private Date dateModified;
	
	
	private static final long serialVersionUID = -3781175036512644090L;

}
