package com.empresa.digital.app.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpresaDigitalUsuariosAppJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpresaDigitalUsuariosAppJavaApplication.class, args);
	}

}
