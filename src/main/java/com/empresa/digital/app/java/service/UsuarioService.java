package com.empresa.digital.app.java.service;

import java.util.List;

import com.empresa.digital.app.java.dto.UsuarioDTO;

public interface UsuarioService {

	public UsuarioDTO save(UsuarioDTO usuario);
	public UsuarioDTO update(UsuarioDTO usuario);
	public void delete (Long id);
	public List<UsuarioDTO> findAll();
	public UsuarioDTO findById(Long id);
	
}
