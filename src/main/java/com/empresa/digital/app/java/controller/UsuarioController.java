package com.empresa.digital.app.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.digital.app.java.dto.UsuarioDTO;
import com.empresa.digital.app.java.service.UsuarioService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/usuarios")
@Validated
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@ApiOperation(
		      value = "Obtiene todos los usuarios.",
		      nickname = "Obtiene todos los usuarios.")
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<UsuarioDTO>> getUsuariosAll(){
		List<UsuarioDTO> usuarioDTOs = usuarioService.findAll();
		return new ResponseEntity<>(usuarioDTOs, HttpStatus.OK);
	}
	
	@ApiOperation(
			value = "Crea nuevos usuarios.",
			nickname = "Crea nuevos usuarios.")
	 @PostMapping(
			  value = "/crear", 
			  produces = {MediaType.APPLICATION_JSON_VALUE})
	  public ResponseEntity<UsuarioDTO> crearUsuario(@RequestBody UsuarioDTO usuario){
		 
		 UsuarioDTO usuarioDTO = usuarioService.save(usuario);
		 
		 return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.CREATED);
	 }
	
	@PutMapping(
			value = "/editar",
			produces = {MediaType.APPLICATION_JSON_VALUE})
	
	public ResponseEntity<UsuarioDTO> editarUsuario(@RequestBody UsuarioDTO usuario) {
		
		UsuarioDTO usuarioDTO =  usuarioService.update(usuario);
		
		return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.CREATED);
	}
	
	@DeleteMapping(
			value = "/eliminar",
			produces = {MediaType.APPLICATION_JSON_VALUE})
	
	public ResponseEntity<UsuarioDTO> eliminarUsuario(@RequestParam Long id) {
		usuarioService.delete(id);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	
	
}
